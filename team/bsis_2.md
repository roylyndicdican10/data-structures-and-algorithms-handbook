# Aspiring Data Analyst

## Alicia Jane Medina

👋 Aspiring Data Analyst!🤵✈️⚽— 💌 aliciajanemedina@student.laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_medina_aliciajane.jpg](images/bsis_2_medina_aliciajane.jpg)

### Bio

**Good to know:** I like drawing. Turning imagination into reality is a spectacular thing! I find information handling, and creating decisions from critical thinking, fun and thrilling. Collecting information and being organized is efficient in doing tasks and keeps me from being insane :)).

**Motto:** Failure is not an option, try until the very end.

**Languages:** Java, SQL

**Other Technologies:** MS Office, ibisPaintX, SketchUp

**Personality Type:** [Advocate (INFJ-T)](https://www.16personalities.com/profiles/b690e0faa45ee)

<!-- END -->

# Aspiring Software Engineer

## Christian Visaya

👋 Aspiring Software Engineer! 🚀👨‍💻 — 💌 christianvisaya@laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_visaya_christian.jpg](images/bsis_2_visaya_christian.jpg)

### Bio

**Good to know:** I'm a fan of optimization. One thing for sure I've learned throughout my career is that communication and understanding is the most significant part of solving a problem. I prefer clarity over speed.

**Motto:** Stability under pressure

**Languages:** Python, Javascript, PHP

**Other Technologies:** AWS, GCP, Microsoft Azure, Digital Ocean, Alibaba Cloud

**Personality Type:** [Protagonist (ENFJ-A)](https://www.16personalities.com/profiles/c5ea44cd4a89a)

<!-- END -->

# Aspiring Web Developer

## Rizalyne Asaldo

👋 Aspiring Web Developer! 👨‍💻 — 💌 rizalyneasaldo@student.laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_asaldo_rizalyne.jpg](images/bsis_2_asaldo_rizalyne.jpg)

### Bio

**Good to know:** i'm a potato lover.

**Motto:** atleast i try.

**Languages:** Java, C#, HTML, Filipino and English.

**Other Technologies:** cellphone saka laptop.

**Personality Type:** [Logistician (ISTJ-T)](https://www.16personalities.com/istj-personality)

<!-- END -->

# Aspiring CEO

## Jerome Imperial

👋 Future CEO!🤵✈️⚽— 💌 jeromeimperial@laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_jerome_imperial.jpg](images/bsis_2_jerome_imperial.jpg)

### Bio

**Good to know:** I have little fear when aiming for towering goals. I might fail, but with God's help, my 'failure' (if you can call it that) will be better than most successes. Nonetheless, I pray for everyone's wellness 😉.

**Motto:** Obsession beats talent🏆🐢..................🐇

**Languages:** Python, Java, C++, SQL

**Other Technologies:** MS Excel, Anaconda's Jupyter Notebook, Arduino UNO

**Personality Type:** [Architect (INTJ-T)](https://www.16personalities.com/profiles/249d8eea60d58) from [Advocate (INFJ-T) ](https://www.16personalities.com/profiles/0d91b1539f64a) 3 years ago.

<!-- END -->

# Aspiring Software Developer

## Christian Eliseo Isip

👋 Aspiring Software Developer! ⌨️🖥️🖱️ — 💌 christianeliseoisip@laverdad.edu.ph — Apalit, Pampanga

![bsis_2_isip_christian.jpg](images/bsis_2_isip_christian.jpg)

### Bio

**Good to know:** I tend to complain a lot whenever things get tough, but I always find a way to get the job done!🫡 To my fellow complainers, turn our complaints into accomplishments🏆✨, one gripe at a time!🙌

**Motto:** Complain first, conquer next!🦾

**Languages:** Java, C++, SQL

**Other Technologies:** MS Word, MS PPT, Canva, VS code, Iphone 14 plus🤓

**Personality Type:** [Advocate (INFJ-T)](https://www.16personalities.com/profiles/c1a9580537bf1)

<!-- END -->

# Aspiring Graduate

## Daniel Latina

👋 Aspiring Graduate! 🚀👨‍💻 — 💌 daniellatina@student.laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_latina_daniel.jpg](images/bsis2_latina_daniel.jpg)

### Bio

**Good to know:** I'm  Bald. One thing for sure I've learned throughout my life is that I'm gettng bald earlier than I anticipated and it's is the most significant part of my life (getting bald). I prefer having hair than being bald.

**Motto:** Preserve hair while they're there

**Languages:** Java, Python, HTML, CSS

**Other Technologies:** VS Code, MySQL

**Personality Type:** [(ISFP-T)](https://www.16personalities.com/infp-personality)

# Aspiring Yaya

## Justine Jynne Patrice A. Marco

🙂 Aspiring Yaya ng mga anak niyo! 🤡❤️‍🔥 —  💬 justinejynnepatricemarco@student.laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_marco_justine.jpg](images/bsis_2_marco_justine.jpg)

### Bio

**Good to know:** I go by many names but hyper is one of them chz na hindi. It's just the way I show my enthusiasm. You may call me Justine or Marco. My favorite color is blue because it's the color of the plants po, mahilig po kasi si Lola sa plants kaya po parati po kami nagbi-beach gusto po niya makita yung ocean.

**Motto:** I will not water myself down to make me more digestible for you. You can choke.

**Languages:** English, Tagalog, Tambay sa Kanto

**Other Technologies:** Pinterest, Instagram

**Personality Type:** [Advocate (INFJ-T)](https://www.16personalities.com/profiles/a94cda034d40b)

<!-- END -->

# Aspiring Degree Holder

## Elloisa Degula

👋 Aspiring Degree Holder! 👨‍💻 — 💌 elloisadegula@student.laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_degula_elloisa.jpg](images/bsis_2_degula_elloisa.jpg)

### Bio

**Good to know:** I love moon and cats a lot. I am quiet and super loud at the same time but it depends to the situation. I'm into sports.

**Motto:** Time is Gold because you cannot bring back time.

**Languages:** Java, C#, mysql

**Other Technologies:** Visual Studio

**Personality Type:** [Campaigner (ENFP-T)](https://www.16personalities.com/profiles/0487b57c646f7)

<!-- END -->

# Aspiring Rich Tita

## Trisha Olorvida

👋 Aspiring Rich Tita!  — 💌 trishamayolorvida@student.laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_olorvida_trisha.jpg](images/bsis_2_olorvida_trisha.jpg)

### Bio

**Good to know:** I'm just a silly human with silly thoughts. I like playing games with friends. I love the color pink and anything cute. I play Genshin Impact (dm for uid). I like listening to music, my current favorite artists/bands are deftones, ptv, mitski, laufey, and zild (medyo emo si ate mo)

**Motto:** Money IS Happiness

**Languages:** HTML, Javascript, Korean, English, Japanese

**Other Technologies:** VSCode, AutoCAD, Adobe, Microsoft

**Personality Type:** [Logician (INTP-T)](https://www.16personalities.com/intp-personality)

<!-- END -->

# Gcash Mod: Unlimited Money APK Developer

## Princess Olingay

💸 Gcash Mod: Unlimited Money APK Developer! 🤑💰 — 💌 princessolingay@student.laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_olingay_princess.jpg](images/bsis_2_olingay_princess.jpg)

### Bio

**Good to know:** Money is like the 6th sense that allows you to enjoy the other 5. *(Satirical)*

**Motto:** When it comes to anything in life-- relationships, friendships, the work you do, the art you make, the music you hear -- when it comes to ***anything***, if it doesn't create an avalanche within your chest, if it doesn't move you and inspire you, if it does not come from the deepest part of who you are, ***it is not for you***.

**Languages:** Basic Java, HTML, MySQL, Money

**Other Technologies:** Microsoft Suite, Google Workspace

**Personality Type:** [Protagonist (ENFJ-T)](https://www.16personalities.com/enfj-personality)

<!-- END -->

# Ambitious IT Professional

## John Miguel Mañabo

:love_you_gesture::peach: Ambitious IT Professional <br> 
:email: johnmiguelmanabo@student.laverdad.edu.ph <br>
:house: Apalit, Pampanga

![bsis_2_manabo_miguel.jpg](images/bsis_2_manabo_miguel.jpg)

## Bio

**Good to know:** *That I am still in the track*

**Motto:** *Long before armament and medicine were invented, there are such words that have powers to save a soul*

**Languages:** *Java, C#, HTML, CSS, JavaScript, Love*

**Other Technologies:** *Unity studios, blender, eclipse, etc.*

**Personality Type:** [Turbulent Consul (ESFJ-T)](https://www.16personalities.com/esfj-personality)
