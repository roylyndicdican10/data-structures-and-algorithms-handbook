# Aspiring Software Engineer

## Christian Visaya

👋 Aspiring Software Engineer! 🚀👨‍💻 — 💌 christianvisaya@laverdad.edu.ph — Apalit, Pampanga

![alt act_2_visaya_christian.jpg](images/bsis_2_visaya_christian.jpg)

### Bio

**Good to know:** I'm a fan of optimization. One thing for sure I've learned throughout my career is that communication and understanding is the most significant part of solving a problem. I prefer clarity over speed.

**Motto:** Stability under pressure

**Languages:** Python, Javascript, PHP

**Other Technologies:** AWS, GCP, Microsoft Azure, Digital Ocean, Alibaba Cloud

**Personality Type:** [Protagonist (ENFJ-A)](https://www.16personalities.com/profiles/c5ea44cd4a89a)

<!-- END -->

# Aspiring Software Developer

## Aikee Apelo

👋 Aspiring Software Developer! 🚀👨‍💻 — 💌 aikeeapelo@laverdad.edu.ph — Apalit, Pampanga

![alt act_2_apelo_aikee.jpg](images/act_2_apelo_aikee.jpg)

### Bio

**Good to know:** I'm a fan of optimization. One thing for sure I've learned throughout my career is that communication and understanding is the most significant part of solving a problem. I prefer clarity over speed.

**Motto:** Even the greatest were beginners

**Languages:** Python, Java,

**Other Technologies:**

**Personality Type:** [Consul ESFJ-T)](https://www.16personalities.com/profiles/c5ea44cd4a89a)

<!-- END -->

# Aspiring Front Desk Technical Support

## Lenie Jane Tinagsa

👋 Aspiring Front Desk Technical Support! 🚀👨‍💻 — 💌 leniejanetinagsa@student.laverdad.edu.ph — Apalit, Pampanga

![alt act_2_tinagsa_leniejane.jpg](<images/act_2_tinagsa_leniejane.jpg>)

### Bio

**Good to know:** I love art things and I love the country of Japan specially their animations.  

**Motto:** Train your mind to see the good in every situation.

**Languages:** Python, Java; Japanese, Portuguese, English (just the basics).

**Other Technologies:** Microsoft Word and Eclipse.

**Personality Type:** [Protagonist (ENFJ-A)](https://www.16personalities.com/profiles/b8d627c287fdc)

# Aspiring To be Everything in the world 

## Kristel Magpayo

👋 Aspiring IT specialist/ Consultant! 🚀✨⚡🔥 — 💌 krystelmagpayo@laverdad.edu.ph — Apalit, Pampanga

![alt act_2_magpayo_kristel.jpg](images\act_2_magpayo_kristel.jpg)
### Bionpm

**Good to know:** Persistence is one trait that I describe for myself. I am resilient and not afraid to get my hands dirty if needed to be. I have many interests, but I tend to be impulsive and not finish what I start. I also love to Travel and have adventures.

**Motto:** Idc about people, I care about other stuff--n'd being sexy**

**Languages:** Python, Java, html

**Other Technologies:** VSCODE, Eclipse, MS Office, basic linux software, Pycharm, clipchamp 

**Personality Type:** [Protagonist (ENTJ-T)](https://www.16personalities.com/profiles/3ad837eacf5eb)

# Aspiring Billionaire HAHAHA

## Jemyll Ramoya

👋 Millionaire or Billionaire — 💌 jemyllramoya@student.laverdad.edu.ph — Apalit, Pampanga

![alt act2_ramoya_jemyll.jpg](images/act2_ramoya_jemyll.jpg)

### Bio

**Good to know:** Better to be unknown, that's good.

**Motto:** Konting bato, konting semento.... Monumento!

**Languages:** English tsaka Tagalog HAHAHA

**Personality Type:** [Architect (INTJ-A)](https://www.16personalities.com/profiles/fd1fc7a8bade9)

# Aspirig Cyber Security

## Rodolfo Ebajan Jr.

👋 Aspiring Cyber Security! 🚀👨‍💻 — 💌 rodolfojrebajan@laverdad.edu.ph — Apalit, Pampanga

![alt act_2_ebajan_rodolfo.jpg](images/act_2_ebajan_rodolfo.jpg)

**Motto:** Pag May Tyaga May Nilaga

**Languages:** Python, Java, HTML

**Other Technologies:**Game Developer

**Personality Type:** [Consul ESFP-A)](https://www.16personalities.com/profiles/c5ea44cd4a89a)

<!-- END -->

# Aspiring Game Developer

## Mark Anthony Lopera

👋 Aspiring Game Developer! 🚀👨‍💻 — 💌 markanthonylopera@laverdad.edu.ph — Apalit, Pampanga

![alt act_2_lopera_markanthony.jpg](images/act_2_lopera_markanthony.jpg)

### Bio

**Good to know:** I'm a fan of optimization. One thing for sure I've learned throughout my career is that communication and understanding is the most significant part of solving a problem. I prefer clarity over speed.

**Motto:** Every learning is a treasure

**Languages:** Python, Java,

**Other Technologies:**software engineer

**Personality Type:** [Consul ESFJ-T)](https://www.16personalities.com/profiles/c5ea44cd4a89a)

<!-- END -->

# Aspiring Nurse

## Kien Marl Trinidad

👋 Aspiring Nurse! 🚀👨‍💻 — 💌 kienmarltrinidad@student.laverdad.edu.ph — Apalit, Pampanga

![alt act_2_trinidad_kienmarl.jpg](images/act_2_trinidad_kienmarl.jpg)

### Bio

**Good to know:**  I'm a fan of Harry Potter since I was Highschool. Sometimes I like to communicate with people and sometimes I do not. I enjoy playing games because I like to receive and deal with challenging task.

**Motto:** Time is Gold!

**Languages:** Python, Javascript, CSS

**Other Technologies:** Visual Studio Code, Git

**Personality Type:** [Consul INFJ-T](https://www.16personalities.com/profiles/c5ea44cd4a89a)

<!-- END -->

# Aspiring Game Developer

## Leo Jay Magistrado

👋 Aspiring Web Developer! 🚀👨‍💻 — 💌 leojaymagistrado@laverdad.edu.ph — Apalit, Pampanga

![alt act_2_magistrado_leojay.jpg](images/act_2_magistrad_leojay.jpg)

### Bio

**Good to know:** I'm a fan of anime 

**Motto:** Time is Gold

**Languages:** Python, Java

**Other Technologies:**

**Personality Type:** [Logistician ISTJ-A)](https://www.16personalities.com/profiles/c5ea44cd4a89a)

<!-- END -->

## Rafael Ramos

👋 Aspiring Game Developer! 🚀👨‍💻 — 💌 rafaelramos@laverdad.edu.ph — Apalit, Pampanga

![alt act_2_ramos_rafael.jpg](images/act_2_ramos_rafael.jpg)

### Bio

**Good to know:** I'm a fan of twice, i'm wife of Son Chaeyoung

**Motto:** Mottolog lang ang motto

**Languages:**

**Other Technologies:**

**Personality Type:** [Protagonist (ENFJ-A)](https://www.16personalities.com/profiles/c5ea44cd4a89a)

<!-- END -->

# Front End Developer

## Rainier Bauca

😎Front End Developer! 🚀👨‍💻 — 💌 rainierbauca@student.laverdad.edu.ph — SanSimon, Pampanga

![alt act_2_bauca_rainier.jpg](images/act_2_bauca_rainier.jpg)

### Bio

**Good to know:** A dedicated K-Pop fan on a journey to become a developer, blending the love for music with the art of coding and Video editing..🍭🍭

**Motto:** You Only Live Once🍭🍭

**Languages:** Python

**Other Technologies:** Adobe

**Personality Type:** [Protagonist (ENFJ-A)](https://www.16personalities.com/profiles/40495ae70faae)

<!-- END -->

# Aspiring CEO

## Roylyn Joy Dicdican

👋 Aspiring CEO! 🚀👨‍💻 — 💌 roylynjoydicdican@laverdad.edu.ph — Apalit, Pampanga

![alt act_2_dicdican_roylyn joy.jpg](images/act_2_dicdican_roylyn_joy.jpg)

### Bio

**Good to know:** Connecting with other people and making friends makes me happy. I'm a soft-hearted person, but I can't show my feelings to others. I love music and the arts.

**Motto:** Consistency can change everybody.

**Languages:** Python, Javascript

**Other Technologies:** 

**Personality Type:** [Protagonist (ENFJ-A)](https://www.16personalities.com/profiles/898ec2297c779)

<!-- END -->
